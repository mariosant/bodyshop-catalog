# Product yamlizer
#
# $ ruby inventory_creator.rb <filename.xlsx>

require 'bundler/setup'
require 'roo'
require 'yaml'
require 'fileutils'
require 'active_support/all'

# Helpers

def sanitize_filename(filename)
  filename.gsub(/[^0-9A-z.\-]/, '_')
end

def find_or_create_dir(dir)
   FileUtils.mkdir_p(dir) unless File.exist?(dir)
end

SPREADSHEET = ARGV[0] || 'russian_inventory.xlsx'
xls = Roo::Excel.new(SPREADSHEET)
xls.default_sheet = xls.sheets[1]

puts "Proccessing file: "

(5..xls.last_row).each do |i|
    product = Hash.new
    product_data_en = Hash.new
    product_data_el = Hash.new

    product["ready"] = xls.cell(i,'A').eql?("ready") ? true : false
    product["article"] = xls.cell(i,'E')
    product["title"] = xls.cell(i,'J')
    product["category"] = xls.cell(i,'G')
    product["child_category"] = xls.cell(i,'F')
    product["images"] = Array.new

    product_data_en["description"] = "#{xls.cell(i,'R')} \n"
    product_data_en["how_to_use"] = "#{xls.cell(i,'T')}\n"
    product_data_en["ingredients"] = xls.cell(i,'V')
    product_data_en["heart_inside"] = xls.cell(i,'X')

    product_data_el["description"] = "#{xls.cell(i,'S')} \n"
    product_data_el["how_to_use"] = "#{xls.cell(i,'U')}\n"
    product_data_el["ingredients"] = xls.cell(i,'W')
    product_data_el["heart_inside"] = xls.cell(i,'Y')

    product["en"] = product_data_en
    product["el"] = product_data_el

    directory = "products/#{product['category']}/#{product['child_category']}"
    file = "#{product['article']}-#{sanitize_filename(product['title']).downcase}.yml"

    find_or_create_dir(directory)
    if !File.exist?("#{directory}/#{file}") && product["ready"]
        File.open("#{directory}/#{file}", 'w') {|f| f.write(product.to_yaml) }
        puts "Saved: #{file}"
    else
        puts "pass #{product['title']}"
    end
end

puts "Proccess complete."