require 'yaml'

class ProductValidator < Sinatra::Base
    get '/' do
        @yamls = parseYamls
        @errors = parseYamls.select {|y| y[:valid] == false}

        erb :index
    end

    private

    def parseYamls
        files = []

        Find.find('products') do |path|
           file = {path: path}
           files << file if path.end_with? '.yml'
        end

        files.map do |file|
            begin
                YAML.load_file(file[:path])
                file[:valid] = true
            rescue Psych::SyntaxError => e
                file[:valid] = false
                file[:error] = e
            ensure
                file[:repo_link] = "https://bitbucket.org/bodyshopgr/bodyshop-catalog/src/master/#{file[:path]}?at=master"
            end
        end

        return files
    end
end